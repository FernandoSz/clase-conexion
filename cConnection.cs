﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tienda
{
     public class cConnection
    {

        private SqlConnection vConexion;
        private String vStringConexion;
        public List<SqlParameter> lstParameters = new List<SqlParameter>();
        //Constructor
        public cConnection(String pServer, String pDataBase, String pUser, String pPassword)
        {
            String vServer = pServer;
            String vDataBase = pDataBase;
            String vUser = pUser;
            String vPassword = pPassword;
            try
            {
                vStringConexion = "Data Source=" + vServer + "; Initial Catalog=" + vDataBase + "; user id=" + vUser + "; Password=" + vPassword;
                vConexion = new SqlConnection(vStringConexion);
            }
            catch(SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        public DataTable mConsultar(String pQuery, bool pProcedure, out String oError)
        {
            SqlCommand vSqlComand = new SqlCommand();
            DataTable vDT = new DataTable();
            try
            {

                if (vConexion.State == System.Data.ConnectionState.Closed) //Verifica si la conexion esta cerrada y la abre
                    vConexion.Open();

                if (pProcedure)
                    vSqlComand.CommandType = CommandType.StoredProcedure;
                //Ejecuta la instruccion
                vSqlComand.CommandText = pQuery;
                vSqlComand.CommandTimeout = 1000;
                vSqlComand.Connection = vConexion;
                foreach (SqlParameter oParameter in lstParameters)
                    vSqlComand.Parameters.Add(oParameter);
                SqlDataAdapter oDa = new SqlDataAdapter(vSqlComand);
                oDa.Fill(vDT);
                oError = "";
                return vDT;

            }
            catch (SqlException ex)
            {
                if (ex.Number != 50000)
                {
                    oError = "Se detecto el siguiente error en la consulta: [" + pQuery + "]" +
                     "\r\n Error: " + ex.Message;
                    return vDT;
                }
                else
                {
                    oError = ex.Message;
                    return vDT;
                }
            }
            finally
            {
                vConexion.Close();
            }

        }




    }
}
